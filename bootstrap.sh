virtualenv .venv
. .venv/bin/activate
pip install https://github.com/platformio/platformio/archive/develop.zip

git submodule init
git submodule update --recursive
find NFC -mindepth 1 -maxdepth 1 -type d -exec ln -nfs $PWD/{} lib \;

if [ ! -d $HOME/.platformio/boards ]; then
    mkdir -p $HOME/.platformio/boards
fi
ln -nfs $PWD/boards/* $HOME/.platformio/boards
